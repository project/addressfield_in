<?php

/**
 * @file
 * All addressfield_in fields management.
 *
 * Page callbacks for adding, editing, and deleting management for
 * addressfield_in.
 */

/**
 * Add new addresses.
 */
function addressfield_in_add() {
  $build = drupal_get_form('addressfield_in_edit_form');
  return $build;
}

/**
 * Edit required address.
 */
function addressfield_in_edit($aid) {
  $address = addressfield_in_get_address_by_aid($aid);
  $build = drupal_get_form('addressfield_in_edit_form', $address);
  return $build;
}

/**
 * Delete unnecessary addresses.
 */
function addressfield_in_delete($aid) {
  $build = drupal_get_form('addressfield_in_delete_form', $aid);
  return $build;
}

/**
 * Implements hook_form().
 */
function addressfield_in_delete_form($form, &$form_state, $aid) {
  $form['aid'] = array(
    '#type' => 'value',
    '#value' => $aid,
  );
  return confirm_form($form, t('Are you sure to delete this address?'), 'addressfield_in', t('The address with ID %aid will be deleted. This action cannot be undone.', array('%aid' => $aid)), t('Delete'), t('Cancel'));
}

/**
 * Implements hook_form_submit().
 */
function addressfield_in_delete_form_submit($form, &$form_state) {
  addressfield_in_address_delete($form_state['values']['aid']);
  drupal_set_message(t('The address with ID %aid has been deleted.', array('%aid' => $form_state['values']['aid'])));
  watchdog('addressfield_in', 'The address with ID %aid has been deleted.', array('%aid' => $form_state['values']['aid']));
  $form_state['redirect'] = 'addressfield-in';
}

/**
 * Function to add and edit address field.
 */
function addressfield_in_edit_form($form, &$form_state, $address = NULL) {
  $max_aid = db_query("SELECT (MAX(aid)+1) FROM addressfield_in")->fetchField();
  $form = array(
    'aid' => array(
      '#title' => t('ID'),
      '#type' => 'textfield',
      '#required' => TRUE,
      '#access' => FALSE,
      '#default_value' => $max_aid,
    ),
    'postal_code' => array(
      '#title' => t('Postal code'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ),
    'dependent_locality' => array(
      '#title' => t('City'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ),
    'locality' => array(
      '#title' => t('Village'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ),
    'sub_administrative_area' => array(
      '#title' => t('District'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ),
    'administrative_area' => array(
      '#title' => t('State'),
      '#type' => 'textfield',
      '#required' => TRUE,
    ),
    'is_edit' => array(
      '#type' => 'value',
      '#value' => 'add',
    ),
    'submit' => array(
      '#type' => 'submit',
      '#value' => t('Save new address'),
    ),
    '#validate' => array('addressfield_in_addresse_form_validate'),
  );
  if (!is_null($address)) {
    $form['aid']['#default_value'] = $address['aid'];
    $form['postal_code']['#default_value'] = $address['postal_code'];
    $form['dependent_locality']['#default_value'] = $address['dependent_locality'];
    $form['locality']['#default_value'] = $address['locality'];
    $form['sub_administrative_area']['#default_value'] = $address['sub_administrative_area'];
    $form['administrative_area']['#default_value'] = $address['administrative_area'];
    $form['submit']['#value'] = t('Save address');
    $form['is_edit'] = array(
      '#type' => 'value',
      '#value' => 'edit',
    );
  }
  return $form;
}

/**
 * Implements hook_form_validate().
 */
function addressfield_in_addresse_form_validate($form, &$form_state) {

}

/**
 * Implements hook_form_submit().
 */
function addressfield_in_edit_form_submit($form, &$form_state) {
  $address = array(
    'aid' => $form_state['values']['aid'],
    'postal_code' => $form_state['values']['postal_code'],
    'locality' => $form_state['values']['locality'],
    'dependent_locality' => $form_state['values']['dependent_locality'],
    'sub_administrative_area' => $form_state['values']['sub_administrative_area'],
    'administrative_area' => $form_state['values']['administrative_area'],
  );
  if (addressefield_in_address_save($address)) {
    if ($form_state['values']['is_edit'] == 'add') {
      drupal_set_message(t('New address has been added.'));
    }
    if ($form_state['values']['is_edit'] == 'edit') {
      drupal_set_message(t('The address with postal code %postal_code and locality %locality have been saved.', array('%postal_code' => $form_state['values']['postal_code'], '%locality' => $form_state['values']['locality'])));
    }
  };

  $form_state['redirect'] = 'addressfield-in';
}
