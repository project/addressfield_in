<?php

/**
 * @file
 * Expose addressfield_in table into view.
 */

/**
 * Implements hook_views_data().
 */
function addressfield_in_views_data() {
  $data = array();
  $data['addressfield_in']['table']['group'] = t('Addressfield_in');
  $data['addressfield_in']['table']['base'] = array(
    'title' => t('Addressfield_in'),
    'help' => t('Contains records we want exposed to Views.'),
  );

  // The aid field.
  $data['addressfield_in']['aid'] = array(
    'title' => t('ID field'),
    'help' => t('The ID field.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // The postalcode field.
  $data['addressfield_in']['postal_code'] = array(
    'title' => t('Postal Code'),
    'help' => t('The postal code.'),
    'field' => array(
      'handler' => 'views_handler_field_numeric',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_numeric',
    ),
  );

  // The City field.
  $data['addressfield_in']['dependent_locality'] = array(
    'title' => t('City'),
    'help' => t('The city name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // The Village field.
  $data['addressfield_in']['locality'] = array(
    'title' => t('Village'),
    'help' => t('The village name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // The District field.
  $data['addressfield_in']['sub_administrative_area'] = array(
    'title' => t('District'),
    'help' => t('The District name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );

  // The State field.
  $data['addressfield_in']['administrative_area'] = array(
    'title' => t('State'),
    'help' => t('The State name.'),
    'field' => array(
      'handler' => 'views_handler_field',
    ),
    'sort' => array(
      'handler' => 'views_handler_sort',
    ),
    'filter' => array(
      'handler' => 'views_handler_filter_string',
    ),
  );
  return $data;
}
