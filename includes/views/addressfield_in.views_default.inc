<?php

/**
 * @file
 * Expose addressfield_in view.
 */

/**
 * Implements hook_views_default_views().
 */
function addressfield_in_views_default_views() {
  // Exported view goes here.
  $view = new view();
  $view->name = 'addressfield_in';
  $view->description = '';
  $view->tag = 'default';
  $view->base_table = 'addressfield_in';
  $view->human_name = 'Addressfield_in';
  $view->core = 7;
  $view->api_version = '3.0';
  $view->disabled = FALSE; /* Edit this to true to make a default view disabled initially */

  /* Display: Master */
  $handler = $view->new_display('default', 'Master', 'default');
  $handler->display->display_options['title'] = 'Addressfield_in';
  $handler->display->display_options['use_more_always'] = FALSE;
  $handler->display->display_options['access']['type'] = 'role';
  $handler->display->display_options['access']['role'] = array(
    3 => '3',
  );
  $handler->display->display_options['cache']['type'] = 'none';
  $handler->display->display_options['query']['type'] = 'views_query';
  $handler->display->display_options['exposed_form']['type'] = 'basic';
  $handler->display->display_options['pager']['type'] = 'full';
  $handler->display->display_options['pager']['options']['items_per_page'] = '50';
  $handler->display->display_options['pager']['options']['offset'] = '0';
  $handler->display->display_options['pager']['options']['id'] = '0';
  $handler->display->display_options['pager']['options']['quantity'] = '9';
  $handler->display->display_options['style_plugin'] = 'table';
  $handler->display->display_options['style_options']['columns'] = array(
    'postal_code' => 'postal_code',
    'locality' => 'locality',
    'dependent_locality' => 'dependent_locality',
    'sub_administrative_area' => 'sub_administrative_area',
    'administrative_area' => 'administrative_area',
    'nothing' => 'nothing',
  );
  $handler->display->display_options['style_options']['default'] = '-1';
  $handler->display->display_options['style_options']['info'] = array(
    'postal_code' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'locality' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'dependent_locality' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'sub_administrative_area' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'administrative_area' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
    'nothing' => array(
      'align' => '',
      'separator' => '',
      'empty_column' => 0,
    ),
  );
  /* Header: Global: Unfiltered text */
  $handler->display->display_options['header']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['content'] = '<a href=/admin/structure/addressfield_in/add>demo</a>';
  /* Field: Addressfield_in: Postal Code */
  $handler->display->display_options['fields']['postal_code']['id'] = 'postal_code';
  $handler->display->display_options['fields']['postal_code']['table'] = 'addressfield_in';
  $handler->display->display_options['fields']['postal_code']['field'] = 'postal_code';
  /* Field: Addressfield_in: City */
  $handler->display->display_options['fields']['dependent_locality']['id'] = 'dependent_locality';
  $handler->display->display_options['fields']['dependent_locality']['table'] = 'addressfield_in';
  $handler->display->display_options['fields']['dependent_locality']['field'] = 'dependent_locality';
  /* Field: Addressfield_in: District */
  $handler->display->display_options['fields']['sub_administrative_area']['id'] = 'sub_administrative_area';
  $handler->display->display_options['fields']['sub_administrative_area']['table'] = 'addressfield_in';
  $handler->display->display_options['fields']['sub_administrative_area']['field'] = 'sub_administrative_area';
  /* Field: Addressfield_in: State */
  $handler->display->display_options['fields']['administrative_area']['id'] = 'administrative_area';
  $handler->display->display_options['fields']['administrative_area']['table'] = 'addressfield_in';
  $handler->display->display_options['fields']['administrative_area']['field'] = 'administrative_area';
  /* Field: Addressfield_in: Village */
  $handler->display->display_options['fields']['locality']['id'] = 'locality';
  $handler->display->display_options['fields']['locality']['table'] = 'addressfield_in';
  $handler->display->display_options['fields']['locality']['field'] = 'locality';
  /* Filter criterion: Global: Fields comparison */
  $handler->display->display_options['filters']['fields_compare']['id'] = 'fields_compare';
  $handler->display->display_options['filters']['fields_compare']['table'] = 'views';
  $handler->display->display_options['filters']['fields_compare']['field'] = 'fields_compare';
  $handler->display->display_options['filters']['fields_compare']['right_field'] = 'postal_code';
  $handler->display->display_options['filters']['fields_compare']['left_field'] = 'postal_code';

  /* Display: Page */
  $handler = $view->new_display('page', 'Page', 'page');
  $handler->display->display_options['defaults']['header'] = FALSE;
  /* Header: Global: Unfiltered text */
  $handler->display->display_options['header']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['header']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['header']['area_text_custom']['content'] = '<a href=/addressfield-in/add>+Create new address</a>';
  $handler->display->display_options['defaults']['empty'] = FALSE;
  /* No results behavior: Global: Unfiltered text */
  $handler->display->display_options['empty']['area_text_custom']['id'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['table'] = 'views';
  $handler->display->display_options['empty']['area_text_custom']['field'] = 'area_text_custom';
  $handler->display->display_options['empty']['area_text_custom']['empty'] = TRUE;
  $handler->display->display_options['empty']['area_text_custom']['content'] = 'No such addresses found';
  $handler->display->display_options['defaults']['fields'] = FALSE;
  /* Field: Global: View result counter */
  $handler->display->display_options['fields']['counter']['id'] = 'counter';
  $handler->display->display_options['fields']['counter']['table'] = 'views';
  $handler->display->display_options['fields']['counter']['field'] = 'counter';
  $handler->display->display_options['fields']['counter']['label'] = 'Serial No.';
  $handler->display->display_options['fields']['counter']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['counter']['counter_start'] = '1';
  $handler->display->display_options['fields']['counter']['reverse'] = 0;
  /* Field: Addressfield_in: ID field */
  $handler->display->display_options['fields']['aid']['id'] = 'aid';
  $handler->display->display_options['fields']['aid']['table'] = 'addressfield_in';
  $handler->display->display_options['fields']['aid']['field'] = 'aid';
  $handler->display->display_options['fields']['aid']['label'] = '';
  $handler->display->display_options['fields']['aid']['exclude'] = TRUE;
  $handler->display->display_options['fields']['aid']['element_label_colon'] = FALSE;
  $handler->display->display_options['fields']['aid']['separator'] = '';
  /* Field: Addressfield_in: Postal Code */
  $handler->display->display_options['fields']['postal_code']['id'] = 'postal_code';
  $handler->display->display_options['fields']['postal_code']['table'] = 'addressfield_in';
  $handler->display->display_options['fields']['postal_code']['field'] = 'postal_code';
  $handler->display->display_options['fields']['postal_code']['separator'] = '';
  /* Field: Addressfield_in: Village */
  $handler->display->display_options['fields']['locality']['id'] = 'locality';
  $handler->display->display_options['fields']['locality']['table'] = 'addressfield_in';
  $handler->display->display_options['fields']['locality']['field'] = 'locality';
  /* Field: Addressfield_in: City */
  $handler->display->display_options['fields']['dependent_locality']['id'] = 'dependent_locality';
  $handler->display->display_options['fields']['dependent_locality']['table'] = 'addressfield_in';
  $handler->display->display_options['fields']['dependent_locality']['field'] = 'dependent_locality';
  /* Field: Addressfield_in: District */
  $handler->display->display_options['fields']['sub_administrative_area']['id'] = 'sub_administrative_area';
  $handler->display->display_options['fields']['sub_administrative_area']['table'] = 'addressfield_in';
  $handler->display->display_options['fields']['sub_administrative_area']['field'] = 'sub_administrative_area';
  /* Field: Addressfield_in: State */
  $handler->display->display_options['fields']['administrative_area']['id'] = 'administrative_area';
  $handler->display->display_options['fields']['administrative_area']['table'] = 'addressfield_in';
  $handler->display->display_options['fields']['administrative_area']['field'] = 'administrative_area';
  /* Field: Global: Custom text */
  $handler->display->display_options['fields']['nothing']['id'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['table'] = 'views';
  $handler->display->display_options['fields']['nothing']['field'] = 'nothing';
  $handler->display->display_options['fields']['nothing']['label'] = 'Links';
  $handler->display->display_options['fields']['nothing']['alter']['text'] = '<a href=/addressfield-in/[aid]/edit>Edit</a>
  <a href=/addressfield-in/[aid]/delete>Delete</a>
  ';
  $handler->display->display_options['fields']['nothing']['alter']['path'] = '/edit';
  $handler->display->display_options['fields']['nothing']['alter']['absolute'] = TRUE;
  $handler->display->display_options['defaults']['filter_groups'] = FALSE;
  $handler->display->display_options['defaults']['filters'] = FALSE;
  /* Filter criterion: Addressfield_in: Postal Code */
  $handler->display->display_options['filters']['postal_code']['id'] = 'postal_code';
  $handler->display->display_options['filters']['postal_code']['table'] = 'addressfield_in';
  $handler->display->display_options['filters']['postal_code']['field'] = 'postal_code';
  $handler->display->display_options['filters']['postal_code']['group'] = 1;
  $handler->display->display_options['filters']['postal_code']['exposed'] = TRUE;
  $handler->display->display_options['filters']['postal_code']['expose']['operator_id'] = 'postal_code_op';
  $handler->display->display_options['filters']['postal_code']['expose']['label'] = 'Postal Code';
  $handler->display->display_options['filters']['postal_code']['expose']['operator'] = 'postal_code_op';
  $handler->display->display_options['filters']['postal_code']['expose']['identifier'] = 'postal_code';
  $handler->display->display_options['filters']['postal_code']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Addressfield_in: Village */
  $handler->display->display_options['filters']['locality']['id'] = 'locality';
  $handler->display->display_options['filters']['locality']['table'] = 'addressfield_in';
  $handler->display->display_options['filters']['locality']['field'] = 'locality';
  $handler->display->display_options['filters']['locality']['group'] = 1;
  $handler->display->display_options['filters']['locality']['exposed'] = TRUE;
  $handler->display->display_options['filters']['locality']['expose']['operator_id'] = 'locality_op';
  $handler->display->display_options['filters']['locality']['expose']['label'] = 'Village';
  $handler->display->display_options['filters']['locality']['expose']['operator'] = 'locality_op';
  $handler->display->display_options['filters']['locality']['expose']['identifier'] = 'locality';
  $handler->display->display_options['filters']['locality']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Addressfield_in: City */
  $handler->display->display_options['filters']['dependent_locality']['id'] = 'dependent_locality';
  $handler->display->display_options['filters']['dependent_locality']['table'] = 'addressfield_in';
  $handler->display->display_options['filters']['dependent_locality']['field'] = 'dependent_locality';
  $handler->display->display_options['filters']['dependent_locality']['group'] = 1;
  $handler->display->display_options['filters']['dependent_locality']['exposed'] = TRUE;
  $handler->display->display_options['filters']['dependent_locality']['expose']['operator_id'] = 'dependent_locality_op';
  $handler->display->display_options['filters']['dependent_locality']['expose']['label'] = 'City';
  $handler->display->display_options['filters']['dependent_locality']['expose']['operator'] = 'dependent_locality_op';
  $handler->display->display_options['filters']['dependent_locality']['expose']['identifier'] = 'dependent_locality';
  $handler->display->display_options['filters']['dependent_locality']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Addressfield_in: District */
  $handler->display->display_options['filters']['sub_administrative_area']['id'] = 'sub_administrative_area';
  $handler->display->display_options['filters']['sub_administrative_area']['table'] = 'addressfield_in';
  $handler->display->display_options['filters']['sub_administrative_area']['field'] = 'sub_administrative_area';
  $handler->display->display_options['filters']['sub_administrative_area']['group'] = 1;
  $handler->display->display_options['filters']['sub_administrative_area']['exposed'] = TRUE;
  $handler->display->display_options['filters']['sub_administrative_area']['expose']['operator_id'] = 'sub_administrative_area_op';
  $handler->display->display_options['filters']['sub_administrative_area']['expose']['label'] = 'District';
  $handler->display->display_options['filters']['sub_administrative_area']['expose']['operator'] = 'sub_administrative_area_op';
  $handler->display->display_options['filters']['sub_administrative_area']['expose']['identifier'] = 'sub_administrative_area';
  $handler->display->display_options['filters']['sub_administrative_area']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  /* Filter criterion: Addressfield_in: State */
  $handler->display->display_options['filters']['administrative_area']['id'] = 'administrative_area';
  $handler->display->display_options['filters']['administrative_area']['table'] = 'addressfield_in';
  $handler->display->display_options['filters']['administrative_area']['field'] = 'administrative_area';
  $handler->display->display_options['filters']['administrative_area']['group'] = 1;
  $handler->display->display_options['filters']['administrative_area']['exposed'] = TRUE;
  $handler->display->display_options['filters']['administrative_area']['expose']['operator_id'] = 'administrative_area_op';
  $handler->display->display_options['filters']['administrative_area']['expose']['label'] = 'State';
  $handler->display->display_options['filters']['administrative_area']['expose']['operator'] = 'administrative_area_op';
  $handler->display->display_options['filters']['administrative_area']['expose']['identifier'] = 'administrative_area';
  $handler->display->display_options['filters']['administrative_area']['expose']['remember_roles'] = array(
    2 => '2',
    1 => 0,
    3 => 0,
  );
  $handler->display->display_options['path'] = 'addressfield-in';
  $handler->display->display_options['menu']['type'] = 'tab';
  $handler->display->display_options['menu']['title'] = 'Administer Indian Address';
  $handler->display->display_options['menu']['weight'] = '0';
  $handler->display->display_options['menu']['name'] = 'devel';
  $handler->display->display_options['menu']['context'] = 1;
  $handler->display->display_options['menu']['context_only_inline'] = 0;

  $views[$view->name] = $view;

  return $views;
}
